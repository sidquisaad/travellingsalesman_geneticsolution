import threading
from multiprocessing.pool import ThreadPool
from typing import List, Union, Tuple

import time

from HankarSidqui import InvalidArgument, AppException, TSPEngine, City


class CSVWriter:
	def __init__(self, filename: str, delimiter: str = ';', append: bool = False):
		if not (isinstance(filename, str)):
			raise InvalidArgument('Expected file to be a string')

		if not (isinstance(delimiter, str) and (len(delimiter) > 0)):
			raise InvalidArgument('Invalid delimiter')

		self.filename = filename
		self.delimiter = delimiter
		self.file = None

		try:
			self.file = open(filename, 'a' if append else 'w')
		except Exception as e:
			raise AppException('File I/O error.\nDetails : {0}'.format(e))

	def is_open(self):
		return True if (self.file is not None) else False

	def close(self):
		if self.file is not None:
			self.file.close()
			self.file = None

	def flush(self):
		if self.file is not None:
			self.file.flush()

	def write_row(self, vals: List[str]):
		if not isinstance(vals, list):
			raise InvalidArgument('Expected list of values')
		s = self.delimiter.join(vals)
		self.file.write(s + '\n')
		return s


def thread_worker(
		name: str,
		cities: Union[Tuple[City, ...], str],
		population_size: int = 50,
		tournament_percentage: float = 0.1,
		mutation_probability: float = 0.1,
		elitism_percentage: float = 0.1,
		generation_limit: int = 2500,
		time_limit: int = 0,
		clone_limit: int = 0
):
	tsp = TSPEngine(
		cities,
		population_size,
		tournament_percentage,
		mutation_probability,
		elitism_percentage,
		generation_limit,
		time_limit,
		clone_limit
	)

	result = tsp.run()
	print('{0} returned : {1}'.format(name, result[1]))
	return result


def do_test(filename: str, thread_count: int = 1, block_repetition: int = 30):
	try:
		if not (isinstance(thread_count, int) and (thread_count > 0)):
			raise InvalidArgument('Expected positive non-null thread count')

		if not (isinstance(block_repetition, int) and (block_repetition > 0)):
			raise InvalidArgument('Expected positive non-null block repetition')

		csv = CSVWriter(filename)

		files_included_in_test = ['pb050.txt', 'pb100.txt', 'pb300.txt']
		city_data = {}

		for file in files_included_in_test:
			city_data[file] = TSPEngine.load_cities_from_file('data/{0}'.format(file))

		x = 0

		for data in city_data:
			for generation_limit in range(2500, 25001, 2500):
				for population_size in range(100, 301, 100):
					for clone_limit in range(50, 251, 100):
						print('==== File : {0}, Generation limit : {1}, Population size : {2}, Clone limit : {3} ===='.format(
							data,
							generation_limit,
							population_size,
							clone_limit
						))

						results = []

						pc = time.perf_counter()
						thread_pool = ThreadPool(thread_count)

						for i in range(block_repetition):
							results.append(thread_pool.apply_async(thread_worker, (
								'Thread {0}'.format(i),
								city_data[data],
								population_size,
								0.1,
								0.1,
								0.1,
								generation_limit,
								0,
								clone_limit
							)))

						thread_pool.close()
						thread_pool.join()

						pc = time.perf_counter() - pc

						columns = [
							'"File : {0}\nGeneration limit : {1}\nPopulation size : {2}\nClone limit : {3}"'.format(
								data,
								generation_limit,
								population_size,
								clone_limit
							),
							str(pc)
						]
						for r in results:
							columns.append(str(r.get()[1]))

						csv.write_row(columns)
						csv.flush()
						print('Done in {0} seconds'.format(pc))

		csv.close()
	except Exception as e:
		raise AppException('Error : {0}'.format(e))


if __name__ == '__main__':
	do_test('tests/output.csv', 8, 10)
