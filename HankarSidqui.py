__author__ = 'Mostapha Hankar & Saad Sidqui'

import pygame
import sys

import math
import random
import time
from typing import Tuple, List, Dict, Callable, Union


# region [Custom exceptions]
class AppException(Exception):
	pass


class InvalidArgument(AppException):
	pass


# endregion

# region [GUI]

class Window:
	COLOR_BLACK = [0, 0, 0]
	COLOR_RED = [175, 0, 0]
	COLOR_GREEN = [0, 175, 0]
	COLOR_BLUE = [0, 0, 175]
	COLOR_WHITE = [255, 255, 255]

	FONT_DEFAULT = None

	def __init__(
			self,
			width: int,
			height: int,
			title: str = '',
			text_color=COLOR_WHITE,
			bg_color=COLOR_BLACK,
			event_loop=True):
		if not (isinstance(width, int) and isinstance(height, int) and (width > 0) and (height > 0)):
			raise InvalidArgument('Expected width and height to be non-null positive integers.')

		if not isinstance(title, str):
			raise InvalidArgument('Expected title to be a string.')

		self.width = width
		self.height = height
		self.title = title
		self.bg_color = bg_color
		self.text_color = text_color

		pygame.init()
		self._handle = pygame.display.set_mode((self.width, self.height), pygame.DOUBLEBUF)
		pygame.display.set_caption(self.title)
		self._surface = pygame.display.get_surface()
		self._surface.fill(self.bg_color)
		pygame.display.flip()
		self._font = pygame.font.SysFont(self.FONT_DEFAULT, 18)
		self.event_loop = False
		self.has_quit = False
		if event_loop:
			self.event_handler()
			pygame.quit()

	def event_handler(self):
		self.event_loop = True
		while self.event_loop:
			evt = pygame.event.wait()
			if evt.type == pygame.QUIT:
				self.event_loop = False
				self.close()

	def close(self):
		self.event_loop = False


class CityInputWindow(Window):
	def __init__(
			self,
			width: int,
			height: int,
			title: str = '',
			text_color=Window.COLOR_WHITE,
			bg_color=Window.COLOR_BLACK,
			city_color=Window.COLOR_GREEN,
			city_radius: int = 5,
	):
		if not (isinstance(city_radius, int) and (city_radius > 0)):
			raise InvalidArgument('Expected city radius to be > 0')

		super().__init__(width, height, title, text_color, bg_color, False)
		self.city_color = city_color
		self.city_radius = city_radius
		self.cities = []
		self._input = True
		self.draw_cities()
		self.collect_cities()
		pygame.quit()

	def draw_cities(self):
		self._surface.fill(self.bg_color)
		for city in self.cities:
			pygame.draw.circle(self._surface, self.city_color, city, self.city_radius)
		info_text = self._font.render(str.format('{0} citie(s)', len(self.cities)), True, self.text_color)
		self._surface.blit(info_text, info_text.get_rect())
		pygame.display.flip()

	def close(self):
		self._input = False
		self.event_loop = False

	def collect_cities(self):
		while self._input:
			evt = pygame.event.wait()
			if evt.type == pygame.QUIT:
				self.has_quit = True
				self.close()
			elif evt.type == pygame.KEYDOWN:
				if evt.key == pygame.K_ESCAPE:
					self.has_quit = True
					self.close()
				elif evt.key == pygame.K_RETURN:
					self._input = False
			elif evt.type == pygame.MOUSEBUTTONDOWN:
				self.cities.append(pygame.mouse.get_pos())
				self.draw_cities()


class ProgressWindow(Window):
	def __init__(
			self,
			width: int,
			height: int,
			title: str = '',
			slowmode: bool = False,
			text_color=Window.COLOR_WHITE,
			bg_color=Window.COLOR_BLACK,
			line_color=Window.COLOR_WHITE,
			city_color=Window.COLOR_GREEN,
			city_radius: int = 5,
	):
		if not isinstance(slowmode, bool):
			raise InvalidArgument('Expected slow mode to be a boolean')

		if not (isinstance(city_radius, int) and (city_radius > 0)):
			raise InvalidArgument('Expected city radius to be > 0')

		super().__init__(width, height, title, text_color, bg_color, False)
		self.slowmode = slowmode
		self.line_color = line_color
		self.city_color = city_color
		self.city_radius = city_radius
		self._input = True

	def close(self):
		pygame.quit()

	def update_callback(self, engine: object, generation: int, current_best: Tuple[Tuple[int], int]) -> bool:
		for evt in pygame.event.get():
			if evt.type == pygame.QUIT:
				return False
			elif evt.type == pygame.KEYDOWN and evt.key == pygame.K_ESCAPE:
					return False

		cities = engine.cities      # type: Tuple[City, ...]
		pos_list = []
		route = current_best[0]

		self._surface.fill(self.bg_color)
		for stop in route:
			pos = (cities[stop].x, cities[stop].y)
			pos_list.append(pos)
			pygame.draw.circle(self._surface, self.city_color, pos, self.city_radius)

		pygame.draw.lines(self._surface, self.line_color, True, pos_list)

		info_text = self._font.render(str.format(
			'Generation {0}, Route Length {1:.2f}', generation, current_best[1]), True, self.text_color)
		self._surface.blit(info_text, info_text.get_rect())
		pygame.display.flip()
		if self.slowmode:
			time.sleep(0.25)
		return True

	def event_handler(self):
		self.event_loop = True
		while self.event_loop:
			evt = pygame.event.wait()
			if evt.type == pygame.QUIT:
				self.event_loop = False
			elif evt.type == pygame.KEYDOWN:
				if evt.key == pygame.K_ESCAPE or evt.key == pygame.K_RETURN:
					self.event_loop = False


# endregion

# region [Genetic Algorithm Engine]
class Utils:
	@staticmethod
	def parse_int(val: str) -> Union[int, None]:
		"""
		Parse a string and returns it's integer value.
		:param val: Value to be parsed
		:return: Integer value of input value, or None if value cannot be parsed
		"""
		try:
			return int(val)
		except ValueError:
			return None


class City:
	def __init__(self, x: int, y: int, name: str):
		if not (isinstance(x, int) and isinstance(y, int)):
			raise InvalidArgument('Expected x and y to be integers.')

		if not isinstance(name, str):
			raise InvalidArgument('Expected name to be a string.')

		self.x = x
		self.y = y
		self.name = name

	def __repr__(self):
		return "Node(name={0.name}, x={0.x}, y={0.y})".format(self)

	def distance_from(self, city: 'City') -> float:
		"""
		Calculates the flying bird distance between the current city and another one.
		:param city: City to calculate the distance from. Should be a valid City object.
		:raises InvalidArgument: If city is not a valid instance of City
		:rtype: float
		:return: Distance between this city and the one provided in argument.
		"""
		if not isinstance(city, City):
			raise InvalidArgument("'city' should be a valid City instance")
		return math.sqrt(math.pow(self.x - city.x, 2) + math.pow(self.y - city.y, 2))

	@staticmethod
	def from_coord_array(coord):
		result = []

		if not (isinstance(coord, list) and (len(coord) > 2)):
			raise InvalidArgument('Expected a list of positions that contains at least three element')

		for i, c in enumerate(coord):
			if not (isinstance(c, tuple) and (len(c) == 2)):
				raise InvalidArgument('Invalid position {0}'.format(c))

			result.append(City(c[0], c[1], 'City {0}'.format(i)))

		result = tuple(result)
		return result


class TSPEngine:
	def __init__(
			self,
			cities: Union[Tuple[City, ...], str],
			population_size: int = 50,
			tournament_percentage: float = 0.1,
			mutation_probability: float = 0.1,
			elitism_percentage: float = 0.1,
			generation_limit: int = 2500,
			time_limit: int = 0,
			clone_limit: int = 0,
			verbose: bool = False
	):
		"""
		Initialises the GA Engine for a TSP problem (incl. population initialisation).
		:param cities: File name or Tuple containing a list of cities. Either should contain at least 2 cities
		:param population_size: Population size. Should be 1 or greater
		:param tournament_percentage: Number of candidates for tournament selection in percentage of population_size
		:param mutation_probability: Probability of mutating a each route leg within a route. 0 = disabled
									Precision beyond 0.xx is not taken into account.
		:param elitism_percentage: Percentage (from the population size) of elite individuals to keep. 0 = disabled
		:param generation_limit: Number of generations after which the engine should stop. 0 = disabled
		:param time_limit: Time limit in second. The engine will stop once this limit is exceeded. 0 = no limit
		:param clone_limit: Number of successive generations where the best routes are identical after which to stop.
							0 = disabled
		:param verbose: If true, display more information messages.
		"""

		if isinstance(cities, str):  # Assuming a file name was passed in
			cities = self.load_cities_from_file(cities)

		if not (isinstance(cities, tuple) and (len(cities) > 2)):
			raise InvalidArgument('Expected a tuple of cities that contains at least three element')

		for city in cities:
			if not isinstance(city, City):
				raise InvalidArgument("{0} is not a valid City instance".format(city))

		if not (isinstance(population_size, int) and isinstance(generation_limit, int) and isinstance(
				time_limit, int) and isinstance(clone_limit, int)):
			raise InvalidArgument(
				'Expected population_size, generation_limit, clone_limit and time_limit to be integers.')

		if not (isinstance(mutation_probability, float) and isinstance(elitism_percentage, float) and isinstance(
				tournament_percentage, float)):
			raise InvalidArgument(
				'Expected mutation_probability, elitism_percentage, and tournament_percentage to be floats')

		if population_size < 1:
			raise InvalidArgument('Expected population_size and generation_limit to be 1 or greater')

		if (mutation_probability < 0) or (mutation_probability > 1) or (elitism_percentage < 0) or (
				elitism_percentage > 1):
			raise InvalidArgument('Expected mutation_probability and elitism_percentage to be between 0 and 1')

		if (tournament_percentage > 1) or (tournament_percentage <= 0):
			raise InvalidArgument('Expected tournament_percentage be between 0 and 1, not null')

		if (time_limit < 0) or (generation_limit < 0) or (clone_limit < 0):
			raise InvalidArgument('Expected generation_limit, clone_limit and time_limit to be positive integers')

		if not isinstance(verbose, bool):
			raise InvalidArgument('Expected verbose to be a boolean')

		self.cities = cities
		self.population_size = population_size
		self.tournament_percentage = tournament_percentage
		self.mutation_probability = mutation_probability
		self.elitism_percentage = elitism_percentage
		self.generation_limit = generation_limit
		self.time_limit = time_limit
		self.clone_limit = clone_limit
		self.verbose = verbose

		self.tournament_places = int(self.tournament_percentage * self.population_size)
		self.mutation_percentage = int(self.mutation_probability * 100)
		self.mutation_range = range(1, self.mutation_percentage) if self.mutation_percentage > 0 else range(0)
		self.elite_amount = int(
			population_size * elitism_percentage) if self.elitism_percentage > 0 else population_size

		self._verbose('Init TSP Engine : {0} cities'.format(len(cities)))
		self._verbose('Population: {0}, Generation limit: {1}'.format(population_size, generation_limit))
		self._verbose(
			'Mutation probability : {0}%, Elite count : {1}'.format(self.mutation_probability, self.elite_amount))

		self.distance_matrix = self.calculate_distance_matrix(self.cities)

		self._verbose('Distance matrix calculated')

		self.routes = self.init_population()
		if self.tournament_places > len(self.routes):
			print('Warning: Tournament candidate count ({0}) is superior to population count ({1}). Corrected.'.format(
				self.tournament_places, len(self.routes)))
			self.tournament_places = len(self.routes)

		if self.tournament_places < 2:
			print('Warning: Tournament candidate count ({0}) is less than 2. Corrected.'.format(
				self.tournament_places, len(self.routes)))
			self.tournament_places = 2

		self._verbose('Initial population generated')
		self.current_best = list(self.routes.items())[0]
		self.current_generation = 0

	def init_population(self) -> Dict[Tuple[int], float]:
		"""
		This function generates a random population based on this object's population_size
		Each route (aka individual) starts from cities[0] and stops at each city once.
		:rtype: Dict[Tuple[int], float]
		:return: A randomly generated population
		"""
		city_indices = list(range(1, len(self.cities)))
		routes = []
		lengths = []

		for i in range(self.population_size):
			available = city_indices.copy()

			# Always depart from the same city
			stops = [0]

			while len(available) > 0:
				choice = random.choice(available)
				del available[available.index(choice)]
				stops.append(choice)

			lengths.append(self.fitness(stops))
			routes.append(tuple(stops))

		return dict(sorted(zip(routes, lengths), key=lambda t: t[1]))

	def fitness(self, stops: List[int]) -> float:
		"""
		Provides a mean of calculating a fitness score for a given route. The longer the route, the higher the returned
		value. Please note that this does not reflect the actual length of a route, as the last leg (that goes from the
		last city to the first city) is not counted in, for the sake of improving performances. The actual length of a
		route is obtainable with calculate_route_length()
		:rtype: float
		:param stops: List of city indices (from self.cities) that make up a route.
		:return: A fitness score.
		"""
		return sum(self.distance_matrix[stops[i]][stops[i - 1]] for i in range(1, len(stops)))

	def calculate_route_length(self, stops: List[int]) -> float:
		"""
		Calculates the total route length
		:rtype: float
		:param stops: List of city indices (from self.cities) that make up a route.
		:return: Route length.
		"""
		return self.fitness(stops) + self.distance_matrix[stops[len(stops) - 1]][stops[0]]

	def select(self) -> List[Tuple[int]]:
		"""
		Selects a couple from the population using Tournament selection
		:return: Couple of (route, route)
		:rtype: List[Tuple[int]]
		"""
		candidates = random.sample(self.routes.items(), self.tournament_places)
		return [candidates[0][0], candidates[1][0]]

	def mutate(self, route: List[int]) -> List[int]:
		"""
		If within the mutation probability, switch the positions of two randomly chosen stops in route
		:param route: List[int]
		:rtype: List[int]
		:return: route
		"""
		if random.randint(1, 100) in self.mutation_range:
			indices = random.sample(list(range(1, len(route))), 2)
			route[indices[0]], route[indices[1]] = route[indices[1]], route[indices[0]]
		return route

	def crossover(self, couple: List[Tuple[int]]) -> List[int]:
		random.shuffle(couple)  # Random 1st parent
		first_parent = couple[0]
		second_parent = couple[1]
		child = [None] * len(first_parent)

		# Select a random part from the 1st parent
		indices = sorted(random.sample(list(range(1, len(first_parent))), 2))
		first_half = first_parent[indices[0]:indices[1]]

		# Copy selected part from first parent to child
		child[indices[0]:indices[1]] = first_half

		index = 0
		for i, stop in enumerate(child):  # For each stop in our new route
			if not stop:  # If this stop is still equal to None
				while True:  # Do :
					candidate = second_parent[index]  # - Choose a new candidate stop from the second parent
					if candidate not in first_half:  # - If the candidate is not in the first_half, then we're good
						break
					index += 1  # - if it is in the firs half, increment the index and try again

				child[i] = candidate
				index += 1
		return child

	def run(self, callback: Callable[[object, int, Tuple[Tuple[int], int]], bool] = None) -> Tuple[Tuple[int], int]:
		"""
		Runs the genetic algorithm, until one of the given constraints are reached.
		:rtype: Tuple[Tuple[int], int]
		:param callback: A callback function that is called each generation.
				callback(engine: TSPEngine, current_generation: int, current_best: Tuple[Tuple[int], int])
		:return: The best solution reached within the given constraint
		"""
		self._verbose('Engine is running ...')
		start = time.monotonic()

		_callback = True if callback is not None else False
		old_best = None
		clone_count = 0

		self.current_generation = 0
		while True:
			old_best = self.current_best
			self._iterate()
			self.current_generation += 1

			if _callback and not callback(self, self.current_generation, self.current_best):
				self._verbose('Callback returned false.')
				break

			if self.clone_limit > 0:
				if old_best[1] == self.current_best[1]:       # Same route length ?
					clone_count += 1
				else:
					clone_count = 0

				if clone_count >= self.clone_limit:
					self._verbose('Clone limit reached ({0}).'.format(clone_count))
					break

			if self.current_generation >= self.generation_limit:
				self._verbose('Generation limit reached ({0}).'.format(self.current_generation))
				break

			if (self.time_limit > 0) and ((time.monotonic() - start) > self.time_limit):
				self._verbose('Time limit reached.')
				break

		self._verbose('Total Generations : {0}'.format(self.current_generation))
		self._verbose('Total duration : {0} seconds'.format(time.monotonic() - start))
		return self.current_best

	def _iterate(self):
		routes = list(self.routes.keys())[:self.elite_amount]  # Select the elite

		while len(routes) < self.population_size:  # Evolution process:
			# - Select a couple, crossover, mutate, add to population
			routes.append(tuple(self.mutate(self.crossover(self.select()))))

		lengths = [self.fitness(route) for route in routes]
		self.routes = dict(sorted(zip(routes, lengths), key=lambda t: t[1]))
		self.current_best = list(self.routes.items())[0]

	def _verbose(self, msg: str):
		if self.verbose:
			print(msg)

	@staticmethod
	def load_cities_from_file(file: str) -> Tuple[City, ...]:
		"""
		Imports cities from a file.
		:rtype: Tuple[City, ...]
		:param file: file path.
		:return: a Tuple of City objects filled with the loade data.
		"""
		cities = []

		if not (isinstance(file, str)):
			raise InvalidArgument('Expected file to be a string')

		try:
			file = open(file, 'r')
			for line in file:
				data = line.split()

				if len(data) != 3:
					raise InvalidArgument(
						'Invalid line "{0}" in file {1} : Expected format NAME X Y'.format(line, file.name))

				x = Utils.parse_int(data[1])
				y = Utils.parse_int(data[2])

				if (x is None) or (y is None):
					raise InvalidArgument('Invalid line "{0}" in file {1} : Expected integers'.format(line, file.name))

				cities.append(City(x, y, data[0]))

		except AppException as ae:
			raise ae
		except Exception as e:
			raise AppException('File could not loaded.\nDetails : {0}'.format(e))
		return tuple(cities)

	@staticmethod
	def calculate_distance_matrix(cities: Tuple[City, ...]) -> Dict[int, Dict[int, int]]:
		"""
		Calculates a distance matrix between each and every two cities.
		:rtype: Dict[int, Dict[int, int]]
		:param cities: Tuple of City objects
		:return: A Dict of Dicts making a distance matrix, indexed by same indexes from cities
		"""
		rows = {}

		# We can optimize further, but since this is a one time process, the improvements are marginal
		# Possible optimization : 0 when src == dest; only calculate src<->dest once
		for i, src in enumerate(cities):
			col = {}
			for j, dest in enumerate(cities):
				col[j] = dest.distance_from(src)
			rows[i] = col
		return rows


# endregion

def ga_solve(
		file: str = None,
		gui: bool = False,
		maxtime: int = 0,
		population: int = 100,
		genlimit: int = 2500,
		clonelimit: int = 0,
		slowmode=False,
		verbose=False
):
	try:
		random.seed()
		if not isinstance(verbose, bool):
			raise InvalidArgument('Expected verbose to be a boolean')

		if not isinstance(gui, bool):
			raise InvalidArgument('Expected gui to be a boolean')

		if not (isinstance(maxtime, int) and (maxtime >= 0)):
			raise InvalidArgument('Expected maxtime to be a positive integer')

		cities = None
		invalid_input = True
		if not (isinstance(file, str) and (len(file) > 0)):  # Show Input GUI
			while invalid_input:
				try:
					input_window = CityInputWindow(500, 500, 'TSP Engine : Input')
					if input_window.has_quit:
						print('Aborted by user.')
						exit(-1)
					cities = City.from_coord_array(input_window.cities)
					invalid_input = False
				except Exception as e:
					print('Please provide a valid input : {0}\n'.format(e))
		else:  # Load from file
			cities = file

		progress_window = None
		callback = None
		if gui:
			progress_window = ProgressWindow(500, 500, 'TSP Engine : Progress', slowmode)
			callback = progress_window.update_callback

		engine = TSPEngine(cities, population, 0.1, 0.2, 0.05, genlimit, maxtime, clonelimit, verbose)
		result = engine.run(callback)
		print('Best Route : {0}\nLength: {1:.2f}'.format(result[0], result[1]))

		if progress_window is not None:
			progress_window.event_handler()
			progress_window.close()

		return result[1], [engine.cities[i].name for i in result[0]]

	except Exception as e:
		print('Error: {0}'.format(e))


def get_arguments(cmd_args: List):
	result = {'gui': True, 'file': None, 'maxtime': 0, 'pop': 100, 'gen': 2500, 'clone': 0, 'slow-mode': False, 'verbose': False}
	skip = True
	for i, arg in enumerate(cmd_args):
		if skip:
			skip = False
			continue

		if (arg == '--help') or (arg == '-h'):
			print('\n')
			print('HankarSidqui.py --help')
			print('HankarSidqui.py [--no-gui] [--maxtime s] [filename] [--population x] [--gen-limit x] [--clone-limit x] [--verbose] [--slow-mode]')
			print('\t-h, --help: Shows this help message')
			print('\t-ng, --no-gui: Hide the progression GUI. An input GUI will still be displayed if no filename is provided')
			print('\t-v, --verbose: Verbose mode')
			print('\t-s, --slow-mode: Enables slow mode, each generation display last a bit longer')
			print('\t-t, --maxtime ss: Duration limit in seconds. Disabled if not provided')
			print('\t-p, --population: Population size')
			print('\t-g, --gen-limit: Generation limit')
			print('\t-c, --clone-limit: Number of successive duplicate after which to stop. Disabled if not provided')
			print(
				'\tfilename: Path to file to load the cities from. If not provided, a GUI will be displayed for input')
			print('\n')
			exit(0)

		elif (arg == '--no-gui') or (arg == '-ng'):
			result['gui'] = False

		elif (arg == '--verbose') or (arg == '-v'):
			result['verbose'] = True

		elif (arg == '--slow-mode') or (arg == '-s'):
			result['slow-mode'] = True

		elif (arg == '--population') or (arg == '-p'):
			try:
				result['pop'] = int(cmd_args[i + 1])
				skip = True
			except Exception:
				print('Error: --population should be followed by an integer')
				exit(-1)

		elif (arg == '--gen-limit') or (arg == '-g'):
			try:
				result['gen'] = int(cmd_args[i + 1])
				skip = True
			except Exception:
				print('Error: --gen-limit should be followed by an integer')
				exit(-1)

		elif (arg == '--clone-limit') or (arg == '-c'):
			try:
				result['clone'] = int(cmd_args[i + 1])
				skip = True
			except Exception:
				print('Error: --clone-limit should be followed by an integer')
				exit(-1)

		elif (arg == '--maxtime') or (arg == '-t'):
			try:
				result['maxtime'] = int(cmd_args[i + 1])
				skip = True
			except Exception:
				print('Error: --maxtime should be followed by an integer')
				exit(-1)

		else:
			result['file'] = arg

	if result['verbose']:
		print('args: {0}'.format(result))
	return result


if __name__ == '__main__':
	args = get_arguments(sys.argv)
	print(ga_solve(args['file'], args['gui'], args['maxtime'], args['pop'], args['gen'], args['clone'], args['slow-mode'], args['verbose']))
