# Travelling Salesman Problem
A genetic solution implemented in Python

## Authors
- Hankar Mostapha
- Sidqui Saad

## Usage

```Batchfile
HankarSidqui.py --help
HankarSidqui.py [--no-gui] [--maxtime s] [filename] [--population x] [--gen-limit x] [--clone-limit x} [--verbose] [--slow-mode]
        --help: Shows this help message
        --no-gui: Hide the progression GUI. An input GUI will still be displayed if no filename is provided
        --verbose: Verbose mode
        --slow-mode: Enables slow mode, each generation display last a bit longer
        --maxtime ss: Duration limit in seconds. Disabled if not provided
        --population: Population size
        --gen-limit: Generation limit
        --clone-limit: Number of successive duplicate after which to stop. Disabled if not provided
        filename: Path to file to load the cities from. If not provided, a GUI will be displayed for input
```

## Python Version
CPython 3.6.4

## Important Note
You will notice that we're using normal Python dict() as ordered dictionaries.
This is because since Python 3.6, dictionaries use an implementation that preserves the order.
[For Python 3.6 this is an implementation detail](https://docs.python.org/3/whatsnew/3.6.html#whatsnew36-compactdict), 
but it was confirmed that it will become a [spec feature in Python 3.7](https://mail.python.org/pipermail/python-dev/2017-December/151283.html).
Hence why we decided to use it.
